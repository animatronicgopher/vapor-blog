import Vapor

let googleAnalyticsString = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-93831889-1', 'auto'); ga('send', 'pageview');"

struct HomeContext: Encodable {
    var googleAnalytics: String
}

public func routes(_ router: Router) throws {
    router.get { req in
        return try req.view().render("home", HomeContext(googleAnalytics: googleAnalyticsString))
    }
}
